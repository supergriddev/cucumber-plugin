const path = require("path");
const vscode = require("vscode");
const vscode_languageclient = require("vscode-languageclient");
function activate(context) {
    // Node server module
    let serverModule = context.asAbsolutePath(path.join('server', 'server.js'));
    //Use debug options for the debug mode
    let debugOptions = { execArgv: ["--nolazy", "--inspect=9229"] };
    let serverOptions = {
        run: { module: serverModule, transport: vscode_languageclient.TransportKind.ipc },
        debug: { module: serverModule, transport: vscode_languageclient.TransportKind.ipc, options: debugOptions }
    };
    let clientOptions = {
        // Register the server for Cucumber feature files
        documentSelector: ['feature'],
        synchronize: {
            configurationSection: 'cucumberautocomplete',
            // Notify the server about file changes to '.clientrc files contain in the workspace
            fileEvents: vscode.workspace.createFileSystemWatcher('**/.clientrc')
        }
    };
    // Create the language client and start the client.
    let disposable = new vscode_languageclient.LanguageClient('cucumberautocomplete-client', 'Cucumber auto complete plugin', serverOptions, clientOptions).start();
    //Client will be deactivate on extension deactivation
    context.subscriptions.push(disposable);
}
exports.activate = activate;
